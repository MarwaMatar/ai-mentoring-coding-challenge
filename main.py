from os.path import expanduser
from models.example_model import ExampleModel
from models.gpt_neo_model import GPTNeoModel
from models.model import Model, ModelObserver
from models.storage_handler import StorageHandler
import argparse
import logging


'''
Requirement1: main.py is used to handel the class GPTNeoModel.
Requirement2: main.py is used to handel the call of ModelObserver.
Requirement3:
            1- Using the Python ```logging``` package to write all program outputs.
            2- Using ```argparse``` to parse command line arguments.
            3- Using "verbose": if set, the log-level should be "DEBUG", if not "INFO".
'''


def start(model: Model, logger):

    observer = ModelObserver(logger)
    #logger.warning("Its a Warning")
    while True:
        user_input = input("\nMax:")
        # check if the user_input == bye, then exit
        try:
            if user_input != 'bye':
                response = model.talk(user_input)
                observer.register(model, model.talk)

                if response is not None:
                    print("\nDonald:{}".format(response))
                else:
                    pass
            else:
                break
        except RuntimeError:
            logger.exception("Exception occurred, please enter a non empty text", exc_info=True)
            pass

    model.dump()



def logging_config(logging_level):
    # Create and configure logger

    logging.basicConfig(filename="loggingFile.log", format='%(asctime)s %(message)s',
                        filemode='w', level=logging_level)
    # Creating an object
    logger = logging.getLogger()

    return logger

def main():
    sh = StorageHandler(expanduser("~"))
    # config the logger
    parser = argparse.ArgumentParser()
    parser.add_argument('-verbose', help="Setting the logging config to debug",  type = bool, required=False)
    args = parser.parse_args()

    if args.verbose:
        logger = logging_config(logging.DEBUG)
    else:
        logger = logging_config(logging.INFO)
    # TODO: change the model to your implementation
    m = GPTNeoModel(sh, logger)
    start(m, logger)




if __name__ == "__main__":
    main()
