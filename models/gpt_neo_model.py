from .model import Model
from transformers import GPTNeoForCausalLM, GPT2Tokenizer
from .storage_handler import StorageHandler
import logging


'''
Requirement 1:
This class achieved the following requirements:
- Sticking to object-oriented programming concepts.
- Implementing the ```talk()```
- Function-signatures weren't changed.
- Using the example provided to me of GPTNeoMode. I just changed what should be changed to meet the requirements.
- Having an ongoing conversation.
- Forbidding Donal to say the word "cool" using the tunning parameters of the generate().
- Including the sentence "If I'm president in 2024 then..." in Trump's 3rd answer should always. I added it to the predicted text.
  please note: I considered the 3rd reply from the beginning of the conversation.
- Handling the instantiatioan and calling of this class from main() in main.py.

extra:
- Separating the loading of EleutherAI/gpt-neo-125M from talk() and handle this according to object-oriented programming concepts.
  This is happened not to load it each time Max and Donal chat.
'''

class GPTNeoModel(Model):
    # TODO: implement this class to predict via GPT Neo
    def __init__(self, storage_handler: StorageHandler, logger):
        super().__init__(storage_handler)

        # I put the initialization here not to be intialized every time we talk or generate the text.
        self.model, self.tokenizer = self._init_transformer()
        self.Donal_3rd_response = True
        self.logger = logger


    def _init_transformer(self):
        prompt = "\n\nThis is a conversation between Max and Donald Trump. They are talking about electric vehicles and how they can make America great again.\n\nMax:I think Tesla is amazing and the US will greatly benefit if we subsidize EVs.\n\nDonald:It’s wonderful to have it as a percentage of your cars, but going fully electric… I think is a mistake.\n\nMax:Why do you think so?\n\nDonald:"
        try:
            model = GPTNeoForCausalLM.from_pretrained("EleutherAI/gpt-neo-125M")
            tokenizer = GPT2Tokenizer.from_pretrained("EleutherAI/gpt-neo-125M")

            input_ids = tokenizer(prompt, return_tensors="pt").input_ids

            gen_tokens = model.generate(input_ids,  do_sample=True, temperature=0.9, max_length=100,
                                        min_length=50, bad_words = ['cool'], top_k=5)
            gen_text = tokenizer.batch_decode(gen_tokens)[0]

            print("\n\n"+gen_text)

        except Exception as e:
            self.logger.error("\n\nException occurred because problem of downloading models ", exc_info=True)

        return model, tokenizer

    def talk(self, user_input: str) -> str:
        gen_text = None
        try:

            input_ids = self.tokenizer(user_input, return_tensors="pt").input_ids
            gen_tokens = self.model.generate(input_ids,  do_sample=True, temperature=0.9, max_length=50,
                                             min_length=50, bad_words = ['cool'], top_k=5)
            gen_text = self.tokenizer.batch_decode(gen_tokens)[0]

            # To handel the 3rd reply for Donal
            if self.Donal_3rd_response:
                gen_text = "If I'm president in 2024 then "+ gen_text
                self.Donal_3rd_response = False
            else:
                pass

        except UnboundLocalError:
            self.logger.exception("\n\nException occurred, please enter a non empty text, so the generated text is empty", exc_info=True)
            pass

        except RuntimeError as e:
            self.logger.exception("Exception occurred, please enter a non empty text, so the generated text is empty", exc_info=True)
            pass
        return gen_text

    def get_content(self):
        return "Some text, some more text, actually a whole conversation of an example model vers. {}...".format(self._version)
