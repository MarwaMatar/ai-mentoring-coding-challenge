from .model import Model
from .storage_handler import StorageHandler


class ExampleModel(Model):

    def __init__(self, storage_handler: StorageHandler):
        super().__init__(storage_handler)

    def talk(self, user_input: str) -> str:
        return "I don't care about user input, i want to make programming great again!"

    def get_content(self):
        return "Some text, some more text, actually a whole conversation of an example model vers. {}...".format(self._version)
