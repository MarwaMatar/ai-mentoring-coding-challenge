from abc import abstractmethod, ABC
from .storage_handler import StorageHandler

class Model(ABC):

    __slots__ = ["_version",
                 "_handler"]

    def __init__(self, handler: StorageHandler):
        self._version = "0.6.19"
        self._handler = handler


    @abstractmethod
    def talk(self, user_input: str) -> str:
        pass

    @abstractmethod
    def get_content(self):
        pass

    def dump(self):
        self._handler.store(self)
'''
Requirement 2:
- This class will be used to observe the talk() method calling and count this call.
'''

class ModelObserver:
    def __init__(self, logger):
        self.callbacks = dict()
        self.count = 0
        self.logger = logger
    '''
    - Here the register can register the calling of talk() or any other function or Observer as required in 2nd requirement.
    - It will count the call of talk
    '''
    def register(self, who, callback=None):
        self.count = self.count + 1
        if callback is None:
            callback = getattr(who, 'talk')
        self.callbacks[who] = callback
        # This logging to tell us the number of calls.
        self.logger.info("\n\nCount of calling register is :{}\n".format(self.count))
